import org.rspeer.runetek.adapter.scene.Pickable;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.script.Script;
import org.rspeer.script.ScriptMeta;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Pickables;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;





@ScriptMeta(desc = "picks axe", name = "axe picker pro", developer = "herrabanani")
public class App extends Script {

    final Position LumbyMine = new Position(3228, 3146, 0);
    final Position InsideDoor = new Position(3229, 3222, 0);
    final Position floor1 = new Position(3229, 3222, 1);
    final Position AxeSpot = new Position(3229, 3218, 2); //the center between the two axes.
    final Position StuckPos = new Position(3229, 3214, 1); //you will get stuck here because it is considered close for some reason, i hard coded a way for the bot to escape :)


    @Override
    public int loop() {

        Player me = Players.getLocal();
        //Pickable axe = Pickables.getNearest(x -> x.getName().equals("Bronze pickaxe") && x.distance(me) < 20 /*&& x.distance(docks) < 20*/);
        Pickable axe = Pickables.getNearest(x -> x.getName().equals("Bronze pickaxe"));
        if (axe != null && !Inventory.isFull() && AxeSpot.distance() <= 5) {
            axe.interact("Take");
            System.out.println("picking axe...");
        }
        else if (AxeSpot.distance(me) > 5)
        {
            Movement.walkTo(AxeSpot);
            System.out.println("going back...");
        }
        else if (me.getPosition().equals(StuckPos))
        {
            System.out.println("oh im stuck again!");
            Movement.walkTo(AxeSpot);
        }
        else
        {
            System.out.println("too close, no need to move");
        }
        if (Inventory.isFull())
        {
            Bank.open();
            System.out.println("going to a bank :)");
            if (Bank.open())
            {
                Time.sleep(500);
                Bank.depositInventory();
                System.out.println("depositing");
            }
        }

        if (!Movement.isRunEnabled() && Movement.getRunEnergy() >= 50)
        {
            Movement.toggleRun(true);
            System.out.println("gotta go fast!");
        }



        Time.sleep(500); //global sleep timer
        return 0;
    }
}